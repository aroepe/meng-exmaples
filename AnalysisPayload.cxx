// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1.h>
#include <TCanvas.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // for histograms
  TH1D *Njet = new TH1D("Njet","Number of Jets",20,0,20);
  TH1D *Mjj = new TH1D("Mjj","Mjj", 100,0,500);

  // TCanvas *c1 = new TCanvas("c1","c1",1000,800);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // for temporary container of jets selected
    std::vector<xAOD::Jet> jets_raw;

    //ANA_MSG_INFO ("execute(): number of jets = " << jets->size());
    //std::cout << "number of jets = " << jets->size() << std::endl;
    Njet->Fill(jets->size());


    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      if(jet->pt()/1000. > 50 && std::abs(jet->eta()) < 2.5){
        jets_raw.push_back(*jet);
      }
    }
    if(jets_raw.size() >= 2){
      Mjj->Fill((jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000.);
    }
    // counter for the number of events analyzed thus far
    count += 1;
  }

  TFile *fout = new TFile("myoutput.root", "RECREATE");
  Njet->Write();
  Mjj->Write();
  fout->Close();
  // c1->cd();
  // Njet->Draw();
  // c1->SaveAs("Njets.pdf");



  // exit from the main function cleanly
  return 0;
}
